= Onrefni =

* by ShomalganD, http://localhost

- this theme is based on Underscores: http://underscores.me/ and Libre: https://theme.wordpress.com/themes/libre/
License: Distributed under the terms of the GNU GPL
Copyright: Automattic, automattic.com

Onrefni is distributed under the terms of the GNU GPL

1.1.9 - November 19, 2018

- fix in the functions.php file for the WooCommerce create_function that is deprecated from PHP 7.2 

1.1.8 - November 12, 2018

- improved support for the new WordPress editor

1.1.7 - August 12, 2018

- added style support for the WPForms

1.1.6 - July 26, 2018

- added basic support for the new WordPress editor

1.1.5 May 23, 2018

- support for the new 4.9.6 WordPress changes regarding GDPR

1.1.4 - May 8, 2018

- small fix for the class-tgm-plugin-activation.php file

1.1.3 - March 26, 2018

- fix for the footer columns (full width column) when just one sidebar is in the use

1.1.2 - January 25, 2018

- one-click demo files added to the theme

1.1.1 - January 8, 2018

- added basic Gutenberg support

1.1.0 - November 20, 2017
- added one-click demo import

1.0.9 - October 5, 2017
- added new page template “Left Sidebar Template” - default template with the left sidebar

1.0.8 - July 18, 2017
- added new option for the blog and single blog page inside the Customizer - disable featured image 
- added post thumbnails image size for the recent post on the front page, blog page and grid page - better website performance 
- woocommerce styles in separate file - css folder

1.0.7 - May 25, 2017

- custom Logo option deprecated and added to core Site Identity section inside the Customizer
- show WooCommerce options in the Customizer only if user activates the WooCommerce   plugin 
- small fix inside the widgets.php file 
- all color options moved to Custom Colors section inside the Customizer 

1.0.6 - April 03, 2017

- added new blank-page.php template - support for the Beaver Builder and Elementor
- small fixes inside the style.css file 


1.0.5 - December 29, 2016

- small fix for the jquery inside the functions.php file

1.0.4 - December 8, 2016

- from WordPress 4.7 Customizer “Custom CSS” section migrated to the core Customizer section called “Additional CSS” - changes inside the inc folder “customizer.php” file


1.0.3 - July 29, 2016

- custom Google Fonts plugin is added to the “plugins” folder inside the main download folder
- added support for the shortcodes inside the excerpt - functions.php file

1.0.2 - June 15,2016

- new tags inside the style.css file
- inside the Customizer - Theme Options new feature “Post Display” - choose full length or excerpt for your blog page
- few changes inside the style.css, author-bio.php and footer.php file - accessibility ready theme

1.0.1 - April 1,2016

- small change inside the ‘inc’ folder widgets.php file - added option to show posts from all categories for the recent post widget
- small fix for the social menu bar inside the style.css file

CREDITS:
1. Genericons: http://genericons.com/ - A free, GPL, flexible icon font
2. Google Fonts: https://www.google.com/fonts 
3. Normalize: /*! normalize.css v3.0.2 | MIT License | git.io/normalize */
4. Codrops: http://tympanus.net/codrops/2015/05/13/inspiration-for-text-styles-and-hover-effects/
