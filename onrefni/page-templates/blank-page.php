<?php
/**
 * Template Name: Page Builder Template
 *
 * @package Onrefni
 * @since Onrefni 1.0.6
 */

get_header( 'custom' ); ?>

	<div class="builder-content">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'page-blank' ); ?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
<?php get_footer( 'custom' ); ?>